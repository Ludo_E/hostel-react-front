import { useEffect, useState } from "react";
import { getStations } from "../../services/stations-requests";
import StationCard from "./StationCard";

import { Alert, AlertType } from "../alert/Alert"

import styles from './StationsManager.module.css'

const StationManager = () => {
    const [stations, setStations] = useState([]);

    useEffect(() => {
        getStations().then(data => {
            setStations(data);
        }).catch(error => {
            console.error(error)
        });
    }, []);

    return (<>
        <h1>Stations</h1>
        <div className={styles.stationsContainer}>
            {stations ? <Alert type={AlertType.ERROR} message="Problème lors de la récupération des stations." /> : stations.map((station) => {
                return <StationCard key={station.id} name={station.name} altitude={station.altitude}></StationCard>
            })}
        </div>
    </>);
}

export default StationManager;