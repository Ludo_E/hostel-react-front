
import styles from './StationCard.module.css';

const StationCard = ({ name, altitude }) => {
    return (<div className={styles.card}>
        <h2>{name}</h2>
        <ul>
            <li>Altitude : {altitude} m</li>
        </ul>
    </div>);
};

export default StationCard;