import styles from "./Alert.module.css";

/**
 * Utilisation d'une classe pour implémenter une énumération.
 * 
 * Les attributs sont "static" et donc accessible sans instance de classe.
 */
class AlertType {
    // Create new instances of the same class as static attributes
    static INFO = new AlertType("INFO");
    static WARNING = new AlertType("WARNING");
    static ERROR = new AlertType("ERROR");

    constructor(name) {
      this.name = name
    }

    toString() {
        return this.name;
    }
  }
  

const Alert = ({type, message}) => {

    /**
     * @param {*} type Le type d'alerte attendu.
     * @returns La classe associée à l'alerte
     */
    function cssClass(type) {
        switch (type.name) {
            case AlertType.WARNING.name:
                return styles.info;
            case AlertType.ERROR.name:
                return styles.error;
            default:
                return styles.info;
        }
    }

    return (<div className={`${styles.alert} ${cssClass(type)}`}>
        {message}
    </div>)
}

export { Alert, AlertType };