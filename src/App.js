import './App.css';
import Header from './components/layout/Header/Header';
import StationManager from './components/stations/StationManager'

function App() {
  return (
    <>
      <Header />
      <main>
        <StationManager />
      </main>
    </>
  );
}

export default App;
