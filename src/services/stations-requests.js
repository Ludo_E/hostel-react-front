// variables url, point d'entrée pour l'API
const url = "http://51.178.19.128:8000/stations";

/**
 * 
 * @returns Une promesse englobant le json représentant les stations.
 */
async function getStations() {
    
    /**
     * Gestion des erreurs avec Try/catch
     */
    try {
        // par défaut le fetch effectue un "GET"
        let res = await fetch(url);
        console.log("Res quoi ? " + res.status );
        if (!res.ok) {
            throw Error(`Erreur lors de la requête : ${res.status} - ${res.statusText}`)
        }

        return await res.json();
    } catch (error) {
        throw Error(`Erreur lors de la requête : ${error}`)
        console.log(error);
    }
}

async function getStationImage(stationId) {
    try {
        // par défaut le fetch effectue un "GET"
        let res = await fetch(url + "/" + stationId + "/image");
        return await res.blob();
    } catch (error) {
        console.log(error);
    }
}

/**
 * Crée une station en effectuant une requête "POST"
 * 
 * @param {*} station Un JSON contenant les informations d'une station 
 */
async function createStation(station) {

    console.log(station);
    // Paramétrage des options pour pouvoir effectuer une méthode "POST"
    // "options" est un JSON !
    let options = {
        method: 'POST',
        body: station
    };

    try {
        let result = await fetch(url, options);

        // la requête http a pu se faire mais il y a une erreur
        if (!result.ok) {
            throw "Erreur lors de la création de la station";
        }

        return await result.json();
    } catch (error) {
        throw "Erreur lors de la création de la station";
    }
}

/**
 * Supprime une station en effectuant une requête "DELETE".
 */
async function deleteStation() {

}

export {getStations, createStation, deleteStation, getStationImage};